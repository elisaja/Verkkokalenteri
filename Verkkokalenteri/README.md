# Ryhmäkalenterisovellus

*Maria Osuala, Elisa Jalava, Helen Tran ja Nea Valtonen*

## Kuvaus

Projektin aiheena on ryhmäkalenterisovellus, joka mahdollistaa ja helpottaa erilaisten ryhmien, kuten perheiden, kavereiden tai projektiryhmien aikataulujen hallinnan ja sovittamisen. Kalenterin käyttäjät voivat merkitä menonsa yksityiseen kalenteriinsa sekä yhden tai useamman ryhmän yhteiseen kalenteriin. Tapahtumien ja tapaamisten, esim. juhlien ja kokoontumisten suunnittelu sekä oman aikataulun hallinta muuttuu näin helpoksi ja vaivattomaksi.

## Tähän asti toteutettu...

Tähän mennessä kalenterin työympäristö on saatu koottua, tietokantayhteydet on saatu toimimaan ja virtuaalikone tietokantapalvelinta varten on valmis käyttöönottoon. Kalenterisovelluksen suunnitteludokumentit on tehty ja tietokanta pystytetty siltä osin, kuin sovellus nykytilassa tarvitsee.  

Kalenterin perusrakenne, käyttöliittymä ja toiminnallisuus on kunnossa.  

Käyttäjätarinat 1-4 ovat valmiita. Kalenteriin voi lisätä merkintöjä ja sovellus tallentaa ne tietokantaan, mutta merkintöjen muokkausta ei ole vielä toteutettu. Sovellukseen voi kirjautua sekä rekisteröityä, ja sovellus tallentaa käyttäjän salasanan kryptattuna tietokantaan bcrypt-algoritmilla. Sovelluksessa on kaksi kielivaihtoehtoa; suomi ja englanti. Ajanpuutteen vuoksi emme ehtineet toteuttaa URL:stä riippuvaa kielenvaihtoa, jonka avulla sivulle saisi kielenvaihtopainikkeen, joten kieli täytyy vaihtaa manuaalisesti asetustiedostoista.

PHPUnit testejä on luotu ja jatkuva integraatio on saatu pystytettyä Jenkinsiin niin, että jokainen git push -komento käynnistää testit Jenkinsissä. Jenkinsissä näkyy testiajojen trendi.

## Arkkitehtuuri

Kalenteri on toteutettu Symfony-nimisen frameworkin tarjoamaa arkkitehtuuria käyttäen.  

Kalenterin toiminnallisuus löytyy src/Acme/KalenteriBundle-kansiosta. Sovellusta ohjaavat php-tiedostot ovat Controller-kansiossa ja sivun ulkoasu Resourses/views/Default-kansiossa. CalendarControllerissa on kalenterin toiminnalle oleelliset metodit ja SecurityControllerissa sisäänkirjautuminen ja rekisteröinti.  

Tietokantayhteyttä hoitavalle Doctrinelle tärkeät, taulujen yhteyksiä ja rakennetta ylläpitävät yaml-tiedostot sijaitsevat Recources/config/doctrine-kansiossa. Entity-kansiosta löytyvät getterit ja setterit, joiden avulla Doctrine muodostaa SQL-lauseita, kun niitä kutsutaan php-tiedostoista.

Sisäänkirjautumisen ja rekisteröitymisen asetukset löytyvät app/config/security.yml-tiedostosta.

Sovelluksen kielen saa vaihdettua app/config/config.yml tiedoston framework:default_locale -kohdasta vaihtamalla kirjoittamalla "fin" tai "en". Kielen vaihtoon on käytetty Symfonyn translate-toimintoa.

Kaikki sovelluksen käyttämät ohjelmat löytyvät versionumeroineen sovelluksen päätasolta, composer.json tiedoston “required” kohdasta.
