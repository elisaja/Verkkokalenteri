<?php
namespace Acme\KalenteriBundle\Controller;
use Acme\KalenteriBundle\Form\MerkintaType;
use Acme\KalenteriBundle\Form\MuokkausType;
use Acme\KalenteriBundle\Entity\Merkinta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;

class CalendarController extends Controller
{
    /**
     * kalenteriAction() renderöi layoutin, joka sivulla näkyy, eli kalenterinäkymän.
     * 
     * Metodi luo sisällään Merkintä-tyyppisen olion. Merkinta() on merkintöjen
     * tietokantaan liittyvä Entity-luokka, jossa määritellään Merkinta-taulun
     * rivit ja koodit.
     * 
     * Kontrolleri luo merkintäoliolle MerkintaType()-luokan avulla <i>lomakkeen</i>,
     * joka tulee näkyviin silloin, kun Luo uusi merkintä -painiketta on painettu.
     * Painikkeen toiminnallisuus on määritelty kalenteri.html.twig-tiedostossa javascriptinä.
     * 
     * Kontrolleri odottaa, kunnes lomake on täytetty, ja tarkistaa sitten
     * isSubmitted() ja isValid() metodien avulla, kelpaavatko tiedot ja onko submit-
     * painiketta painettu. Kun tämä on tehty, kontrolleri settaa Merkinta-entityyn
     * merkinnän päivämäärän ja merkinnän tehneen käyttäjän ID:n (joita ei täytetty lomakkeessa)
     * ja lähettää lopulta entityn täytettyine tietoineen Doctrinelle, joka lähettää SQL-kyselyn
     * mySQLle automaattisesti.
     * Doctrinen getManager()-metodi avaa tietokantayhteyden, persist() lisää SQL-lauseen
     * entity-olion avulla
     * ja flush() lähettää sen.
     * 
     * Kommentoitu koodi on pohja kalenterimerkinnän muokkausta varten, joka lähestulkoon 
     * toimi muttei kuitenkaan tarpeeksi, ja aiheutti liikaa
     * ongelmia ohjelmassa. SItä ei olla kokonaan poistettu koska sitä saatetaan
     * tarvita mallina.
     * 
     * @param Request $request
     * @return type
     * @Route("/kalenteri", name="_kalenteri")
     */
     public function kalenteriAction(Request $request){
        $merkkaus = new Merkinta();     
        $form = $this->createForm(new MerkintaType(), $merkkaus);   
     
        $em= $this->getDoctrine()->getManager();
        
  
        if ($request->request->has($form->getName())) {
            $form->handleRequest($request);
            if ($form->isSubmitted()) {

                // Haetaan tämänhetkinen käyttäjä
                $user = $this->getUser();
                $userId = $user->getId();

                $kayttaja = $this->getDoctrine()
                        ->getRepository('AcmeKalenteriBundle:Kayttaja')
                        ->find($userId);

                $merkkaus->setKayttaja($kayttaja);
                //tallennetaan tietokantaan   
                $em->persist($merkkaus);
                $em->flush();
            }
        }
       /* if ($request->request->has($form2->getName())){
            
            $form2->handleRequest($request);
            
            $title = $this->get('request')->request->get('title');
            $kuvaus = $this->get('request')->request->get('kuvaus');
            $aika = $this->get('request')->request->get('aika');
            $start = $this->get('request')->request->get('start');
            $nakyvyys = $this->get('request')->request->get('kuvaus');
            if ($form2->isSubmitted()){
               $muokkaus->setTitle($title);
               $muokkaus->setKuvaus($kuvaus);
               $muokkaus->setAika($aika);
               $muokkaus->setStart($start);
               $muokkaus->setNakyvyys($nakyvyys);
               $em->flush();
            }
        }
        */
        return $this->render('AcmeKalenteriBundle:Default:kalenteri.html.twig',
                              array('newnote' => $form->createView(),
                                ));
     }
    
    /**
     *DateQueryAction on kontrolleri Ajax-kyselylle, joka tapahtuu kalenteri.html.twig-
     * tiedostossa. DateQuery hakee Doctrinen avulla tietokannasta nykyisen käyttäjän
     * id:n getID()-metodin avulla. Nykyisen käyttäjän IDtä käytetään löytämään merkinnät,
     * jotka kuuluvat juuri tälle käyttäjälle. Näin varmistetaan yksityisyys ja se,
     * ettei kalenterissa näy vääriä merkintöjä.
     * 
     * Doctrine suorittaa SQL-kyselyn ja hakee tulokset json-formaatissa oliolistana.
     * Kontrolleri palauttaa listan Response-tyyppisenä, eli ajax saa responsen 
     * palvelimelta, mutta käyttäjä ei itse pääse suoraan katselemmaan sitä.
     * kalenteri.html.twigissä ajax ottaa vastaan responsen ja muuttaa json-palautteen
     * listaksi, jonka FullCalendar lukee kalenterimerkinnöiksi.
     * 
     * @return
     * @Route("/datequery", name="_datequery")
     */
    
    public function DateQueryAction(){
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new GetSetMethodNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            
            // Haetaan tämänhetkinen käyttäjä
            $user = $this->getUser();
            $userId = $user->getId();

            $em = $this->getDoctrine()->getEntityManager();
            $query = $em->createQuery(
                    "SELECT m.title, m.start FROM AcmeKalenteriBundle:Merkinta m WHERE m.kayttaja='" . $userId . "'");
            $array = $query->getResult();
            $array=$serializer->serialize($array, 'json');
            //$return=array("responseCode"=>200,  "responseArray"=>$array);
            //$return=json_encode($return);
            return new Response($array);
           
    }
    
    /**
     *
     * @return
     * @Route("/eventquery", name="_eventquery")
     */
    
    public function EventQueryAction(Request $request){
        $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new GetSetMethodNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
        $em = $this->getDoctrine()->getManager();
        
        $merkinta= $request->get('variable');
        //$dss = $repository->findByStart($merkinta);
        //$dss= json_encode($dss);
        $query= $em->createQuery("SELECT m.title, m.start, m.kuvaus, m.aika, m.nakyvyys FROM AcmeKalenteriBundle:Merkinta m WHERE m.start='$merkinta'");
        $array=$query->getResult();
        $return=$serializer->serialize($array, 'json');
        return new Response($return);     
    }
   
}

