<?php
namespace Acme\KalenteriBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

//include (dirname(__DIR__).'/Resources/views/Default/kalenteri.html.twig');

class DefaultController extends Controller
{
    /**
     * Action()-metodit määrittävät sen, mitä tapahtuu, kun metodin nimeen kuuluva route avataan.
     * Routet on määritelty routing.yml-tiedostossa.
     * Actionin nimi on tärkeä kirjoittaa oikein, sillä se näkyy routing.yml -tiedostossa
     * defaults-rivillä.
     * 
     * indexaAction() metodina renderöi index.html.twig-tiedoston sivulle.
     * 
     * @return type
     * @Route("/home", name="_home")
     */
    public function indexAction()
    {
        return $this->render('AcmeKalenteriBundle:Default:index.html.twig');
    }

}
