<?php
namespace Acme\KalenteriBundle\Controller;
use Acme\KalenteriBundle\Form\UserType;
use Acme\KalenteriBundle\Entity\Kayttaja;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    /**
     * loginAction() on kontrolleri ohjelman login-sivulle.
     * metodi käyttää symfonyn omaa turvallisuusjärjestelmää authenticationUtilsia
     * käyttäjän autentoinnissa. $authenticationUtils tarkistaa, onko annettu tunnus
     * ja salasana samanlaiset tietokannan rivin kanssa (login-sivulle on määritetty
     * provider eli tietokantayhteys security.yml-tiedostossa).
     * 
     * Mikäli tulee error, authenticationUtils palauttaa viimeksi tulleen errorin kontrollerille,
     * jolloin sen voi lähettää näkymään.
     * AuthenticationUtils löytää myös viimeisimmän käyttäjän syöttämän käyttäjänimen.
     * 
     * loginAction renderöi näkymän ja mahdollisesti näkyvän virheilmoituksen sivulle.
     * 
     * @param Request $request
     * @return type
     * @Route("/login", name="_login")
     * @Template()
     */
    public function loginAction(Request $request){
        
        $session = $request->getSession();

        // Get the login error if there is one
        
        // Pois kommentoitua ei tarvita, ellei uudelleenkonfiguroi sitä, miten Symfony lähettää
        // login-sivulle; use_forward, joka saa Symfonyn forwardaamaan login sivulle eikä redirectaamaan sinne
        
//        if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
//            $error = $request->attributes->get(
//                SecurityContextInterface::AUTHENTICATION_ERROR
//            );
//        } else {
        $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
        $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);

        return
            array(
                // last username entered by the user
                'last_username' => $session->get(SecurityContextInterface::LAST_USERNAME),
                'error'         => $error,
            )
        ;
        
    }
    
    /**
     * loginCheckActionin avulla Symfony tarkistaa, onko kirjautumistiedot oikein ja ohjaa oikealle sivulle.
     * 
    * @Route("/login_check", name="_login_check")
    */
    public function loginCheckAction()
    {
    }
    
    /**
     * logOutAction ohjaa käyttäjän oikealle sivulle uloskirjautuessa.
     * 
    * @Route("/logout", name="_logout")
    */
    public function logoutAction()
    {
    }
    
    /**
     * registerAction on näkymä rekisteröintisivulle.
     * 
     * Kuten kalenteriAction, registerAction sisältää myös lomakkeen luonnin ja täytön
     * sekä tietokantaan tietojen lähetyksen Doctrinen avulla. 
     * Lomake muodostetaan ja lähetetään UserType-luokan ja Kayttaja-entityn avulla.
     * 
     * if-lause tarkistaa ensin, että onko lomake lähetetty, ja enkoodaa sen jälkeen
     * security.yml-tiedostossa määritetyn enkooderin avulla.
     * Enkoodattu salasana asetetaan kayttis-olion salasanaksi.
     * 
     * Kun enkoodaus on tehty kontrolleri käyttää Doctrinea lähettämään SQL-lauseen
     * tietokantaan ja uudelleenohjaa 
     * 
     * @param Request $request
     * @return type
     * @Route("/register", name="_register")
     */
    public function registerAction (Request $request){
        $kayttis = new Kayttaja();
        $form = $this->createForm(new UserType(), $kayttis);
        
        //handle submit
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            
            //encode password
            $password =$this->get('security.password_encoder')
                    ->encodePassword($kayttis, $kayttis->getSalasana());
            $kayttis->setSalasana($password);
            
            //tallenna käyttäjä
            $em = $this->getDoctrine()->getManager();
            $em->persist($kayttis);
            $em->flush();
            
            return $this->redirectToRoute('_home');
            
        }
        return $this->render(
                'AcmeKalenteriBundle:Default:register.html.twig',
                array('form' => $form->createView())
                );
    }
    
}
