<?php
namespace Acme\KalenteriBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MuokkausType extends AbstractType{
    
     public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
                ->add('title', 'text')
                ->add('kuvaus', 'text')
                
                ->add('aika', 'time', array(
                    'input' => 'string'
                ))
                ->add('nakyvyys', 'choice', array(
                        'choices'  => array(
                            'Salattu' => 'salattu',
                            'Piilotettu' => 'piilotettu',
                            'Julkinen' => 'julkinen',
                        )))           
                ->add('start', 'text');        
                        
            
    }
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'Acme\KalenteriBundle\Entity\Merkinta',
        ));
    }
    public function getName() {
        return 'title';
    }


    
    }
