<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Cya!</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css') }}">
        <script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js') }}"></script>
        <script src="{{ asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js') }}"></script>
        <link href="<?php echo base_url('/css/etusivu.css'); ?>" type="text/css" rel="stylesheet" />
        <link href="<?php echo base_url('/css/navbars.css'); ?>" type="text/css" rel="stylesheet" />
    </head>
    <body class = "tausta">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Verkkokalenteri</a>
                    <a class="navbar-brand" href="#">Profiili</a>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Kirjaudu ulos</a></li>
                </ul>
            </div>

        </nav>