<?php

namespace Acme\KalenteriBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase {
    
    /**
     * Testaa indexAction()-metodia ja katsoo, onnistuuko renderöinti oikealle sivulle.
     * Onnistuessaan sen pitäisi palauttaa true.
     * 
     * @covers Acme\KalenteriBundle\Controller\DefaultController::indexAction
     * @todo   Implement testIndexAction().
     */
    public function testIndexAction() {
        
        $client = static::createClient();
        
        //haetaan sivu
        $client->request('GET', '/home');
        $response = $client->getResponse();
        
        //onko tilakoodi sama kuin 302
        $this->assertEquals('302', $response->getStatusCode());
        
    }
}
