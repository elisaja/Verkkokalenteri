<?php

namespace Acme\KalenteriBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Merkinta
 */
class Merkinta
{
    /**
     * @var integer
     * 
     */
    private $merkintaId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $kuvaus;

    /**
     * @var string
     */
    private $aika;

    /**
     * @var string
     */
    private $start;

    /**
     * @var string
     */
    private $nakyvyys;

    /**
     * @var \Acme\KalenteriBundle\Entity\Kayttaja
     */
    private $kayttaja;


    /**
     * Get merkintaId
     *
     * @return integer 
     */
    public function getMerkintaId()
    {
        return $this->merkintaId;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Merkinta
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get nimi
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set kuvaus
     *
     * @param string $kuvaus
     * @return Merkinta
     */
    public function setKuvaus($kuvaus)
    {
        $this->kuvaus = $kuvaus;

        return $this;
    }

    /**
     * Get kuvaus
     *
     * @return string 
     */
    public function getKuvaus()
    {
        return $this->kuvaus;
    }

    /**
     * Set aika
     *
     * @param string $aika
     * @return Merkinta
     */
    public function setAika($aika)
    {
        $this->aika = $aika;

        return $this;
    }

    /**
     * Get aika
     *
     * @return string 
     */
    public function getAika()
    {
        return $this->aika;
    }

    /**
     * Set start
     *
     * @param string $start
     * @return Merkinta
     */
    public function setStart($paiva)
    {
        $this->start = $paiva;

        return $this;
    }

    /**
     * Get start
     *
     * @return string 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set nakyvyys
     *
     * @param string $nakyvyys
     * @return Merkinta
     */
    public function setNakyvyys($nakyvyys)
    {
        $this->nakyvyys = $nakyvyys;

        return $this;
    }

    /**
     * Get nakyvyys
     *
     * @return string 
     */
    public function getNakyvyys()
    {
        return $this->nakyvyys;
    }

    /**
     * Set kayttaja
     *
     * @param \Acme\KalenteriBundle\Entity\Kayttaja $kayttaja
     * @return Merkinta
     */
    public function setKayttaja(\Acme\KalenteriBundle\Entity\Kayttaja $kayttaja)
    {
        $this->kayttaja = $kayttaja;

        return $this;
    }

    /**
     * Get kayttaja
     *
     * @return \Acme\KalenteriBundle\Entity\Kayttaja 
     */
    public function getKayttaja()
    {
        return $this->kayttaja;
    }
}
