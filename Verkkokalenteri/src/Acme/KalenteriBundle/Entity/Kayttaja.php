<?php

namespace Acme\KalenteriBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Kayttaja
 */
class Kayttaja implements UserInterface, \Serializable
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $tunnus;

    /**
     * @var string
     */
    private $salasana;

    /**
     * @var string
     */
    private $nimi;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $kuvaus;

    /**
     * @var string
     */
    private $puhelin;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tunnus
     *
     * @param string $tunnus
     * @return Kayttaja
     */
    public function setTunnus($tunnus)
    {
        $this->tunnus = $tunnus;

        return $this;
    }

    /**
     * Get tunnus
     *
     * @return string 
     */
    public function getTunnus()
    {
        return $this->tunnus;
    }

    /**
     * Set salasana
     *
     * @param string $salasana
     * @return Kayttaja
     */
    public function setSalasana($salasana)
    {
        $this->salasana = $salasana;

        return $this;
    }

    /**
     * Get salasana
     *
     * @return string 
     */
    public function getSalasana()
    {
        return $this->salasana;
    }

    /**
     * Set nimi
     *
     * @param string $nimi
     * @return Kayttaja
     */
    public function setNimi($nimi)
    {
        $this->nimi = $nimi;

        return $this;
    }

    /**
     * Get nimi
     *
     * @return string 
     */
    public function getNimi()
    {
        return $this->nimi;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Kayttaja
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set kuvaus
     *
     * @param string $kuvaus
     * @return Kayttaja
     */
    public function setKuvaus($kuvaus)
    {
        $this->kuvaus = $kuvaus;

        return $this;
    }

    /**
     * Get kuvaus
     *
     * @return string 
     */
    public function getKuvaus()
    {
        return $this->kuvaus;
    }

    /**
     * Set puhelin
     *
     * @param string $puhelin
     * @return Kayttaja
     */
    public function setPuhelin($puhelin)
    {
        $this->puhelin = $puhelin;

        return $this;
    }

    /**
     * Get puhelin
     *
     * @return string 
     */
    public function getPuhelin()
    {
        return $this->puhelin;
    }
    
    /**
     * Hakee käyttäjän roolin
     * 
     * @return Role
     */
    
    // USER INTERFACEN TARVITSEMIA METODEITA
    
    public function getRoles() { 
        return array('ROLE_USER');
    }
    
    /**
     * Hakee tavan, jolla salasana on enkoodattu. Koska käytämme bcryptiä, salttia ei tarvita ja palautetaan null.
     * 
     * @return null
     */
    
    public function getSalt() {
        return null;
    }

    /**
     * Pyyhkii pääsytiedot
     */
    
    public function eraseCredentials()
    {
    }
    
    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->tunnus,
            $this->salasana,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->tunnus,
            $this->salasana,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }

    public function getPassword() {
        return $this->salasana;
    }

    public function getUsername() {
        return $this->tunnus;
    }

}